# Dow Adv File Format Editor (FFE) #
### Base ###
* Low level tool similar to the vanilla File Format with additional features.
* Tool supports the following file types : sgb, whm, whe, rgd, rsh, events
### Detailed features: ###
* edit chunk data (ID, Version and Name)
* cut/copy/paste chunks
* reorder chunks
* Hex edit DATA chunks + cut, copy paste, delete, undo and redo buttons.
* Search chunks (IDs, Names, text content, Hex. content)
* Replace text content (option is included to update String lengths)
## [Download section](https://bitbucket.org/dark40k/dowadvfileeditor/downloads) ##

![FFEv1.00.png](https://bitbucket.org/repo/andxK4/images/3637896974-FFEv1.00.png)

[File Format Editor thread on Relic Forum](http://forums.relicnews.com/showthread.php?264719-File-Format-Editor-v0-4-(Updated-3-Feb-12)&p=1045331348#post1045331348)