package fr.dow.sgeditor.chunkeditor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JOptionPane;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.sgeditor.FileFormatEditorUndoMgr;

public class ChunkEditor extends ChunkEditorBase {

	FileFormatEditorUndoMgr undoMgr;
	
	RelicChunk chunk;

	public ChunkEditor(FileFormatEditorUndoMgr undoMgr) {
		super();
		panelHexEditor.setVisible(false);
		this.undoMgr=undoMgr;
	}

	public void setChunk(RelicChunk newChunk) {

		saveChunk();
		chunk = newChunk;
		
		if (newChunk == null) {
			clear();
			panelHexEditor.setVisible(false);
			return;
		}

		panelHexEditor.setVisible(chunk.get_type().equals(RelicChunkDATA.type));
		
		updateChunkData();
		
	}

	public void updateChunkData() {

		if (chunk == null) return;
		
		textFieldType.setText(chunk.get_type());
		textFieldChunkHeaderSize.setText(chunk.get_headersize().toString());
		textFieldID.setText(chunk.get_id());
		textFieldChunkContentSize.setText(chunk.get_datasize().toString());
		textFieldVersion.setText(chunk.get_version().toString());
		textFieldName.setText(chunk.get_name());
		
		if (chunk.get_type().equals(RelicChunkDATA.type)) {
			try {
				hexEditor.open(new ByteArrayInputStream(((RelicChunkDATA) chunk).dataArray()));
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public RelicChunk getChunk() {
		return chunk;
	}

	public void clearNameSelection() {
		textFieldName.setSelectionStart(0);
		textFieldName.setSelectionEnd(0);		
	}
	
	public void setNameSelection(String stringPattern) {
		int selStart = textFieldName.getText().indexOf(stringPattern);
		if (selStart<0) {
			clearNameSelection();
			return;
		}
		textFieldName.setSelectionStart(selStart);
		textFieldName.setSelectionEnd(selStart+stringPattern.length());
		textFieldName.requestFocus();
	}
	
	public byte[] getSelectionCopy() {
		
		if (hexEditor.getLargestSelectionIndex()==0) return null;
		return Arrays.copyOfRange(hexEditor.getData(), hexEditor.getSmallestSelectionIndex(), hexEditor.getLargestSelectionIndex()+1);
		
	}
			
	public String getSelectionAsString() {
		
		if (chunk == null) return "";
		
		if (getHexSelectionEnd()<=0) return "";
		
		byte[] block=getSelectionCopy();
		for (int i=0; i<block.length; i++) 
			if (block[i]<0x20 || block[i]>0x7e) 
				block[i]=0x2e;
		
		return new String (block);
	}
	
	public String getSelectionAsHexString() {
		
		if (chunk == null) return "";
		
		if (getHexSelectionEnd()<=0) return "";
		
		String hexString="";
		
		byte[] block=getSelectionCopy();
		for (int i=0; i<block.length; i++) { 
			int b = block[i] & 0xff;
			if (b<0x10) hexString = hexString +"0"+Integer.toHexString(b);
			else hexString = hexString + Integer.toHexString(b);
		}
		return hexString;
	}
	
	public void replaceSelection(byte[] newData) {
		hexEditor.replaceSelection(newData);
	}
	
	public int getHexSelectionStart() {
		
		if (chunk==null) return -1;
		if (!chunk.get_type().equals(RelicChunkDATA.type)) return -1;
		
		return hexEditor.getSmallestSelectionIndex();

	}
	
	public int getHexSelectionEnd() {
		
		if (chunk==null) return -1;
		if (!chunk.get_type().equals(RelicChunkDATA.type)) return -1;
		
		return hexEditor.getLargestSelectionIndex();

	}

	public void setHexSelection(int startOffs, int endOffs) {
		if (chunk==null) return;
		if (!chunk.get_type().equals(RelicChunkDATA.type)) return;
		hexEditor.setSelectedRange(startOffs, endOffs);
	}
	
	public void saveChunk() {
		if (chunk == null) return; 

		Boolean hasChanged = false;
		hasChanged = hasChanged || !textFieldName.getText().equals(chunk.get_name());
		hasChanged = hasChanged || !textFieldVersion.getText().equals(chunk.get_version().toString());

		if (hasChanged) undoMgr.addUndoableGlobalEdit("Edit chunk " + chunk.get_id());

		chunk.set_name(textFieldName.getText());
		chunk.set_version(new Integer(textFieldVersion.getText()));
		
		if (!chunk.get_type().equals(RelicChunkDATA.type)) return;
		
		if (!hasChanged)
			if (!Arrays.equals(((RelicChunkDATA) chunk).dataArray(),hexEditor.getData()))
				undoMgr.addUndoableGlobalEdit("Edit chunk " + chunk.get_id());
		
		((RelicChunkDATA) chunk).dataWrapArray(hexEditor.getData());
		((RelicChunkDATA) chunk).updateSize();
	}
	
	public void clear() {

		chunk = null;

		textFieldType.setText(null);
		textFieldChunkHeaderSize.setText(null);
		textFieldID.setText(null);
		textFieldChunkContentSize.setText(null);
		textFieldVersion.setText(null);
		textFieldName.setText(null);

	}

	@Override
	protected void doCut() {
		hexEditor.cut();
	}

	@Override
	protected void doCopy() {
		hexEditor.copy();
	}

	@Override
	protected void doPaste() {
		hexEditor.paste();
	}

	@Override
	protected void doDelete() {
		hexEditor.delete();
	}

	@Override
	protected void doUndo() {
		hexEditor.undo();
	}

	@Override
	protected void doRedo() {
		hexEditor.redo();
	}

	@Override
	protected void doSelectionTextEdit() {
		
		if (hexEditor.getLargestSelectionIndex()==0) return;
		
		byte[] block=getSelectionCopy();
		
		for (int i=0;i<block.length;i++) 
			if (block[i]<0x20 || block[i]>0x7e) {
				JOptionPane.showMessageDialog(this, "Selection must be full text.","Selection Error",JOptionPane.ERROR_MESSAGE);
				return;
			}
		
		String  oldText=new String(block);
		
		String s = (String) JOptionPane.showInputDialog(this, "Selection="+oldText, "Edit text selection",
				JOptionPane.PLAIN_MESSAGE, null, null, oldText);
		if (s == null) return;
		
		replaceSelection(s.getBytes());
		
	}

	@Override
	protected void doAutoTextEdit() {
		
		if (hexEditor.getLargestSelectionIndex() == 0) return;

		byte[] bytedata = hexEditor.getData();

		int startIndex = hexEditor.getSmallestSelectionIndex();
		
		if (hexEditor.getLargestSelectionIndex() - hexEditor.getSmallestSelectionIndex() < 3) {
			while (startIndex > 3) {
				if (bytedata[startIndex] == 0x00) break;
				startIndex--;
			}
			startIndex = startIndex - 3;
			if (startIndex < 0) {
				JOptionPane.showMessageDialog(this, "Initial 4 bytes length not found.", "Selection Error",JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		int size = bytedata[startIndex + 3] << 24 | (bytedata[startIndex + 2] & 0xff) << 16
				| (bytedata[startIndex + 1] & 0xff) << 8 | (bytedata[startIndex + 0] & 0xff);

		if ((size<0)||((startIndex + 4 + size)>=bytedata.length)) {
			JOptionPane.showMessageDialog(this, "Initial 4 bytes length not found.", "Selection Error",JOptionPane.ERROR_MESSAGE);
			return;
		}

		hexEditor.setSelectedRange(startIndex, startIndex + size + 3);

		for (int i = startIndex + 4; i < (startIndex + 4 + size); i++)
			if (bytedata[i] < 0x20 || bytedata[i] > 0x7e) {
				JOptionPane.showMessageDialog(this, "Selection must be full text.", "Selection Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

		String oldText = new String(bytedata, startIndex + 4, size);

		String s = (String) JOptionPane.showInputDialog(this, "Selection=" + oldText, "Edit text selection",
				JOptionPane.PLAIN_MESSAGE, null, null, oldText);
		if (s == null) return;

		byte[] newData=new byte[s.length()+4];
		newData[0]=(byte) ((s.length() & 0x000000FF) >> 0);
		newData[1]=(byte) ((s.length() & 0x0000FF00) >> 8);
		newData[2]=(byte) ((s.length() & 0x00FF0000) >> 16);
		newData[3]=(byte) ((s.length() & 0xFF000000) >> 24);
		System.arraycopy(s.getBytes(), 0, newData, 4, s.length());
		
		replaceSelection(newData);

	}


}
