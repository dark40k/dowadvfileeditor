package fr.dow.sgeditor.chunkeditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import fr.dow.hexeditor.swing.HexEditor;


public abstract class ChunkEditorBase extends JPanel {
	
	protected JTextField textFieldType;
	protected JTextField textFieldChunkHeaderSize;
	protected JTextField textFieldID;
	protected JTextField textFieldChunkContentSize;
	protected JTextField textFieldVersion;
	protected JTextFieldWithTrailingZero textFieldName;
	protected HexEditor hexEditor;
	protected JButton btnDelete;
	protected JButton btnPaste;
	protected JButton btnCut;
	protected JButton btnCopy;
	protected JPanel panelHexEditor;
	private JSeparator separator;
	private JButton btnUndo;
	private JButton btnRedo;
	private Component horizontalStrut_1;
	private Component rigidArea;
	private Component rigidArea_1;
	private JButton btnRawTextEdit;
	private JButton btnAutoSelText;

	/**
	 * Create the panel.
	 */
	public ChunkEditorBase() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelChunkData = new JPanel();
		panelChunkData.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Chunk Header Data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panelChunkData, BorderLayout.NORTH);
		GridBagLayout gbl_panelChunkData = new GridBagLayout();
		gbl_panelChunkData.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelChunkData.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panelChunkData.columnWeights = new double[]{0.0, 0.0, 4.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelChunkData.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelChunkData.setLayout(gbl_panelChunkData);
		
		rigidArea = Box.createRigidArea(new Dimension(10, 10));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 0;
		panelChunkData.add(rigidArea, gbc_rigidArea);
		
		JLabel label = new JLabel("Type:");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		panelChunkData.add(label, gbc_label);
		
		textFieldType = new JTextField();
		textFieldType.setEditable(false);
		textFieldType.setColumns(10);
		GridBagConstraints gbc_textFieldType = new GridBagConstraints();
		gbc_textFieldType.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldType.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldType.gridx = 2;
		gbc_textFieldType.gridy = 1;
		panelChunkData.add(textFieldType, gbc_textFieldType);
		
		horizontalStrut_1 = Box.createHorizontalStrut(10);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 3;
		gbc_horizontalStrut_1.gridy = 1;
		panelChunkData.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JLabel label_1 = new JLabel("Chunk Header Size:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 4;
		gbc_label_1.gridy = 1;
		panelChunkData.add(label_1, gbc_label_1);
		
		textFieldChunkHeaderSize = new JTextField();
		textFieldChunkHeaderSize.setEditable(false);
		textFieldChunkHeaderSize.setColumns(10);
		GridBagConstraints gbc_textFieldChunkHeaderSize = new GridBagConstraints();
		gbc_textFieldChunkHeaderSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChunkHeaderSize.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChunkHeaderSize.gridx = 5;
		gbc_textFieldChunkHeaderSize.gridy = 1;
		panelChunkData.add(textFieldChunkHeaderSize, gbc_textFieldChunkHeaderSize);
		
		JLabel label_2 = new JLabel("ID:");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 2;
		panelChunkData.add(label_2, gbc_label_2);
		
		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setColumns(10);
		GridBagConstraints gbc_textFieldID = new GridBagConstraints();
		gbc_textFieldID.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldID.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldID.gridx = 2;
		gbc_textFieldID.gridy = 2;
		panelChunkData.add(textFieldID, gbc_textFieldID);
		
		JLabel label_3 = new JLabel("Chunk Contents Size:");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 4;
		gbc_label_3.gridy = 2;
		panelChunkData.add(label_3, gbc_label_3);
		
		textFieldChunkContentSize = new JTextField();
		textFieldChunkContentSize.setEditable(false);
		textFieldChunkContentSize.setColumns(10);
		GridBagConstraints gbc_textFieldChunkContentSize = new GridBagConstraints();
		gbc_textFieldChunkContentSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChunkContentSize.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChunkContentSize.gridx = 5;
		gbc_textFieldChunkContentSize.gridy = 2;
		panelChunkData.add(textFieldChunkContentSize, gbc_textFieldChunkContentSize);
		
		JLabel label_4 = new JLabel("Version:");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.WEST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 1;
		gbc_label_4.gridy = 3;
		panelChunkData.add(label_4, gbc_label_4);
		
		textFieldVersion = new JTextField();
		textFieldVersion.setColumns(10);
		GridBagConstraints gbc_textFieldVersion = new GridBagConstraints();
		gbc_textFieldVersion.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldVersion.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldVersion.gridx = 2;
		gbc_textFieldVersion.gridy = 3;
		panelChunkData.add(textFieldVersion, gbc_textFieldVersion);
		
		JLabel label_6 = new JLabel("Name:");
		label_6.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.EAST;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 1;
		gbc_label_6.gridy = 4;
		panelChunkData.add(label_6, gbc_label_6);
		
		textFieldName = new JTextFieldWithTrailingZero();
		textFieldName.setColumns(10);
		GridBagConstraints gbc_textFieldName = new GridBagConstraints();
		gbc_textFieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldName.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldName.gridx = 2;
		gbc_textFieldName.gridy = 4;
		panelChunkData.add(textFieldName, gbc_textFieldName);
		
		rigidArea_1 = Box.createRigidArea(new Dimension(10, 10));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.gridx = 6;
		gbc_rigidArea_1.gridy = 5;
		panelChunkData.add(rigidArea_1, gbc_rigidArea_1);
		
		panelHexEditor = new JPanel();
		add(panelHexEditor);
		panelHexEditor.setLayout(new BorderLayout(0, 0));
		
		JPanel panelHexTools = new JPanel();
		panelHexEditor.add(panelHexTools, BorderLayout.NORTH);
		GridBagLayout gbl_panelHexTools = new GridBagLayout();
		gbl_panelHexTools.columnWidths = new int[]{33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelHexTools.rowHeights = new int[]{0, 0};
		gbl_panelHexTools.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelHexTools.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panelHexTools.setLayout(gbl_panelHexTools);
		
		btnCut = new JButton("");
		btnCut.setToolTipText("Cut");
		btnCut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doCut();
			}
		});
		btnCut.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/cut.gif")));
		GridBagConstraints gbc_btnCut = new GridBagConstraints();
		gbc_btnCut.anchor = GridBagConstraints.WEST;
		gbc_btnCut.insets = new Insets(0, 0, 0, 5);
		gbc_btnCut.fill = GridBagConstraints.VERTICAL;
		gbc_btnCut.gridx = 1;
		gbc_btnCut.gridy = 0;
		panelHexTools.add(btnCut, gbc_btnCut);
		
		btnCopy = new JButton("");
		btnCopy.setToolTipText("Copy");
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doCopy();
			}
		});
		btnCopy.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/copy.gif")));
		GridBagConstraints gbc_btnCopy = new GridBagConstraints();
		gbc_btnCopy.anchor = GridBagConstraints.WEST;
		gbc_btnCopy.fill = GridBagConstraints.VERTICAL;
		gbc_btnCopy.insets = new Insets(0, 0, 0, 5);
		gbc_btnCopy.gridx = 2;
		gbc_btnCopy.gridy = 0;
		panelHexTools.add(btnCopy, gbc_btnCopy);
		
		btnPaste = new JButton("");
		btnPaste.setToolTipText("Paste");
		btnPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doPaste();
			}
		});
		btnPaste.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/paste.gif")));
		GridBagConstraints gbc_btnPaste = new GridBagConstraints();
		gbc_btnPaste.anchor = GridBagConstraints.WEST;
		gbc_btnPaste.fill = GridBagConstraints.VERTICAL;
		gbc_btnPaste.insets = new Insets(0, 0, 0, 5);
		gbc_btnPaste.gridx = 3;
		gbc_btnPaste.gridy = 0;
		panelHexTools.add(btnPaste, gbc_btnPaste);
		
		btnDelete = new JButton("");
		btnDelete.setToolTipText("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doDelete();
			}
		});
		btnDelete.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/delete.gif")));
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 0, 0, 5);
		gbc_btnDelete.fill = GridBagConstraints.VERTICAL;
		gbc_btnDelete.gridx = 4;
		gbc_btnDelete.gridy = 0;
		panelHexTools.add(btnDelete, gbc_btnDelete);
		
		separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.fill = GridBagConstraints.VERTICAL;
		gbc_separator.insets = new Insets(0, 0, 0, 5);
		gbc_separator.gridx = 5;
		gbc_separator.gridy = 0;
		panelHexTools.add(separator, gbc_separator);
		
		btnUndo = new JButton("");
		btnUndo.setToolTipText("Undo");
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doUndo();
			}
		});
		btnUndo.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/undo.gif")));
		GridBagConstraints gbc_btnUndo = new GridBagConstraints();
		gbc_btnUndo.insets = new Insets(0, 0, 0, 5);
		gbc_btnUndo.gridx = 6;
		gbc_btnUndo.gridy = 0;
		panelHexTools.add(btnUndo, gbc_btnUndo);
		
		btnRedo = new JButton("");
		btnRedo.setToolTipText("Redo");
		btnRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doRedo();
			}
		});
		btnRedo.setIcon(new ImageIcon(ChunkEditorBase.class.getResource("/icons/redo.gif")));
		GridBagConstraints gbc_btnRedo = new GridBagConstraints();
		gbc_btnRedo.insets = new Insets(0, 0, 0, 5);
		gbc_btnRedo.gridx = 7;
		gbc_btnRedo.gridy = 0;
		panelHexTools.add(btnRedo, gbc_btnRedo);
		
		btnRawTextEdit = new JButton("Selection Text Edit");
		btnRawTextEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doSelectionTextEdit();
			}
		});
		GridBagConstraints gbc_btnRawTextEdit = new GridBagConstraints();
		gbc_btnRawTextEdit.insets = new Insets(0, 0, 0, 5);
		gbc_btnRawTextEdit.gridx = 9;
		gbc_btnRawTextEdit.gridy = 0;
		panelHexTools.add(btnRawTextEdit, gbc_btnRawTextEdit);
		
		btnAutoSelText = new JButton("Auto Text Edit");
		btnAutoSelText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doAutoTextEdit();
			}
		});
		GridBagConstraints gbc_btnAutoSelText = new GridBagConstraints();
		gbc_btnAutoSelText.gridx = 10;
		gbc_btnAutoSelText.gridy = 0;
		panelHexTools.add(btnAutoSelText, gbc_btnAutoSelText);
		
		hexEditor = new HexEditor();
		panelHexEditor.add(hexEditor, BorderLayout.CENTER);

	}

	protected abstract void doCut();

	protected abstract void doCopy();

	protected abstract void doPaste();

	protected abstract void doDelete();

	protected abstract void doUndo();

	protected abstract void doRedo();
	
	protected abstract void doSelectionTextEdit();
	
	protected abstract void doAutoTextEdit();
	
}
