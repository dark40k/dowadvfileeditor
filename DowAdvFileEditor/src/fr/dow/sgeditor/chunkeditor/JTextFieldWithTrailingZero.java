package fr.dow.sgeditor.chunkeditor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JTextFieldWithTrailingZero extends JPanel {
	
	private final static String zeroCar=Character.toString((char) 0);
	
	private JTextField textField;
	private JButton button;
	
	public JTextFieldWithTrailingZero() {
		setLayout(new BorderLayout(0, 0));
		
		button = new JButton("00");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				button.setSelected(!button.isSelected());
			}
		});
		add(button, BorderLayout.EAST);
		
		textField = new JTextField();
		add(textField, BorderLayout.CENTER);
		textField.setColumns(10);
	}

	 /**
	  * @see JTextField#setColumns(int columns)
	  */
	public void setColumns(int columns) {
		textField.setColumns(columns);
	}
	
	 /**
	  * @see JTextField#getText()
	  */
	public String getText() {
		return textField.getText()+(button.isSelected()?zeroCar:"");
	}

	
	 /**
	  * @see JTextField#setText(String t)
	  */
	public void setText(String t) {
		
		if (t==null) {
			textField.setText(null);
			button.setSelected(false);
			return;
		}
		
		if (t.endsWith(zeroCar)) {
			button.setSelected(true);
			t=t.substring(0, t.length()-1);
		} else 
			button.setSelected(false);
		
		textField.setText(t);
	}

	 /**
	  * @see JTextField#setSelectionStart(int selectionStart)
	  */
	public void setSelectionStart(int selectionStart) {
		textField.setSelectionStart(selectionStart);
	}

	 /**
	  * @see JTextField#setSelectionEnd(int selectionEnd)
	  */
	public void setSelectionEnd(int selectionEnd) {
		textField.setSelectionEnd(selectionEnd);
	}

}
