package fr.dow.sgeditor;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.TreePath;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkFile;
import fr.dow.sgeditor.chunkeditor.ChunkEditor;
import fr.dow.sgeditor.find.FindChunkHeader;
import fr.dow.sgeditor.find.FindChunkHex;
import fr.dow.sgeditor.find.FindChunkText;
import fr.dow.sgeditor.find.ReplaceChunkText;
import fr.dow.sgeditor.treechunk.TreeChunk;
import fr.dow.sgeditor.treechunk.TreeChunkAdressPanel;
import fr.dow.sgeditor.treechunk.TreeChunkNode;

public class FileFormatEditor implements TreeSelectionListener {

	private static final String windowTitle="File Format Editor v1.05 - ";
	
	private static final FileNameExtensionFilter dowChunkFileFilter =
			new FileNameExtensionFilter("Dawn of War Chunk File (sga,sgb,sgm,ebp,whm,whe,rgd,rsh,events)", 
			"sgb", "sga", "ebp", "sgm", "whm", "whe", "rgd", "rsh", "events");
	
	private static final String dowRawChunkExtension = ".chk";
	private static final FileNameExtensionFilter dowRawChunkFileFilter =
			new FileNameExtensionFilter("Dawn of War Raw Chunk File (chk)", "chk");
	
	private static File startFile;
	private static File currentDirectory;
	
	private JFrame frmFileFormatEditor;

	private ChunkEditor chunkEditor;
	private TreeChunk treeChunk;

	private File openedFile;
	private RelicChunkFile chunkFile;

	private FileFormatEditorUndoMgr undoMgr = new FileFormatEditorUndoMgr(this);

	private TreeChunkAdressPanel addressPanel;
	
	private final static Preferences userPreferences=Preferences.userRoot().node(FileFormatEditor.class.getName());
	
	// TODO Added transfer handler
	private static final TransferHandler DEFAULT_TRANSFER_HANDLER = new FileFormatEditorTransferHandler();
	
	// ---------------------------------------------------------------------------------------- 
	//
	// Constructeur
	//
	// ---------------------------------------------------------------------------------------- 

	public FileFormatEditor(File startFile) {
		initialize();
		if (startFile != null) openFile(startFile);
	}
	
	// ---------------------------------------------------------------------------------------- 
	//
	// Getters
	//
	// ---------------------------------------------------------------------------------------- 

	public FileFormatEditorUndoMgr getUndoMgr() {
		return undoMgr;
	}
	
	// ---------------------------------------------------------------------------------------- 
	//
	// Methodes
	//
	// ---------------------------------------------------------------------------------------- 

	static void setCurrentDirectory(File newDirectory) {
		if (newDirectory == null) return;
		if (!newDirectory.isDirectory()) return;
		currentDirectory = newDirectory;
		userPreferences.put("directory", newDirectory.getPath());
	}

	private void openFile(File fileToOpen) {
		
		// check if file exists
		if (!fileToOpen.exists()) {
			System.err.println("File not found");
			return;
		}

		setCurrentDirectory(fileToOpen.getParentFile());

		try {
			chunkFile = new RelicChunkFile(fileToOpen);
		}
		catch (IOException e) {
			System.err.println("File could not be loaded");
			e.printStackTrace();
			return;
		}

		undoMgr.setChunkFile(chunkFile);
		
		openedFile = fileToOpen;
		frmFileFormatEditor.setTitle(windowTitle+fileToOpen.getAbsolutePath());

		treeChunk.setChunkFile(fileToOpen.getName(), chunkFile);
		chunkEditor.setChunk(null);

		setCurrentDirectory(fileToOpen.getParentFile());

	}
	
	public void saveAs(File sgFile) {

		chunkEditor.saveChunk();

		try {
			chunkFile.save(sgFile);
		}
		catch (IOException e) {
			System.err.println("Failed to save file");
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not save file", "Warning", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		openedFile = sgFile;
		frmFileFormatEditor.setTitle(windowTitle+openedFile.getAbsolutePath());
		treeChunk.setBaseName(openedFile.getName());
		
		setCurrentDirectory(sgFile.getParentFile());
		
		JOptionPane.showMessageDialog(null, "File saved", "Information", JOptionPane.INFORMATION_MESSAGE);

	}

	public TreeChunk getTreeChunk() {
		return treeChunk;
	}
	
	public ChunkEditor getChunkEditor() {
		return chunkEditor;
	}
		
	// ---------------------------------------------------------------------------------------- 
	//
	// Commandes de l'interface
	//
	// ---------------------------------------------------------------------------------------- 
	
	public void commandMenuOpen() {

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(dowChunkFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			System.err.println("No file selected");
			return;
		}

		openFile(sgFile);

	}

	public void commandMenuExit() {
		System.exit(0);
	}

	public void commandMenuSave() {
		saveAs(openedFile);
	}

	public void commandMenuSaveAs() {

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(dowChunkFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			System.err.println("No file selected");
			return;
		}

		setCurrentDirectory(sgFile.getParentFile());

		saveAs(sgFile);
	}

	public void commandEditChunkEditUndo() {
		if (undoMgr.canUndo()) undoMgr.undo();
	}
	
	public void commandEditChunkEditRedo() {
		if (undoMgr.canRedo()) undoMgr.redo();
	}
	
	public void commandEditChunkDelete() {
		
		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;
		
		TreeChunkNode fatherNode = chunkNode.getFather();
		if (fatherNode==null) return;
		
		Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
		TreePath selectionBackup = treeChunk.getSelectedPath();

		undoMgr.addUndoableGlobalEdit("Delete "+chunkNode.getChunk().get_name());
		
		int index = fatherNode.getIndexOfChild(chunkNode);
		if (index>0)
			selectionBackup = selectionBackup.getParentPath().pathByAddingChild(fatherNode.getChildAt(index-1));
		else
			selectionBackup = selectionBackup.getParentPath();
		
		fatherNode.deleteChunk(chunkNode);
		
		treeChunk.forceUpdate();

		treeChunk.loadExpansionState(expansionStateBackup);
		treeChunk.setSelectedPath(selectionBackup);

	}

	public void commandEditChunkCut() {
		if (treeChunk.getLastSelectedNode() == null) return;
		invokeAction(TransferHandler.getCutAction());
	}

	public void commandEditChunkCopy() {
		if (treeChunk.getLastSelectedNode() == null) return;
		invokeAction(TransferHandler.getCopyAction());
	}

	public void commandEditChunkPaste() {
		if (treeChunk.getLastSelectedNode() == null) return;
		invokeAction(TransferHandler.getPasteAction());
	}

	public void commandEditChunkExportRaw() {

		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(dowRawChunkFileFilter);

		chooser.setSelectedFile(new File(chunkNode.getTreePath() + dowRawChunkExtension));
		
		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			System.err.println("No file selected");
			return;
		}

		setCurrentDirectory(sgFile.getParentFile());

		sgFile= sgFile.getName().endsWith(dowRawChunkExtension) ? sgFile : new File(sgFile.getPath()+dowRawChunkExtension);
		
		try {
			FileOutputStream outputStream=new FileOutputStream(sgFile);
			chunkNode.getChunk().write(outputStream);
			outputStream.close();
		}
		catch (IOException e) {
			System.err.println("Failed to save file");
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not save file", "Warning", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		JOptionPane.showMessageDialog(null, "File saved", "Information", JOptionPane.INFORMATION_MESSAGE);

	}

	public void commandEditChunkImportRaw() {

		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;
		
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(dowRawChunkFileFilter);

		int res = chooser.showOpenDialog(null);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			System.err.println("No file selected");
			return;
		}
		
		
		// check if file exists
		if (!sgFile.exists()) {
			System.err.println("File not found");
			return;
		}

		setCurrentDirectory(sgFile.getParentFile());

		RelicChunk relicChunk;
		
		try {
			relicChunk = RelicChunk.loadChunk(new BufferedInputStream(new FileInputStream(sgFile)));
		}
		catch (IOException e) {
			System.err.println("File could not be loaded");
			e.printStackTrace();
			return;
		}
		
		if (relicChunk == null) return;

		Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
		TreePath selectionBackup = treeChunk.getSelectedPath();

		undoMgr.addUndoableGlobalEdit("Import raw chunk");
		
		chunkNode.pasteChunk(relicChunk);
		treeChunk.forceUpdate();

		treeChunk.loadExpansionState(expansionStateBackup);
		treeChunk.setSelectedPath(selectionBackup);

	}

	public void commandEditChunkMoveUp() {

		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;

		chunkEditor.saveChunk();

		Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
		TreePath selectionBackup = treeChunk.getSelectedPath();

		undoMgr.addUndoableGlobalEdit("Move Chunk Up");
		
		chunkNode.moveUp();
		treeChunk.forceUpdate();

		treeChunk.loadExpansionState(expansionStateBackup);
		treeChunk.setSelectedPath(selectionBackup);

	}

	public void commandEditChunkMoveDown() {

		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;

		chunkEditor.saveChunk();

		Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
		TreePath selectionBackup = treeChunk.getSelectedPath();

		undoMgr.addUndoableGlobalEdit("Move Chunk Down");
		
		chunkNode.moveDown();
		treeChunk.forceUpdate();

		treeChunk.loadExpansionState(expansionStateBackup);
		treeChunk.setSelectedPath(selectionBackup);

	}

	public void commandEditChunkEditID() {

		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode == null) return;
		if (chunkNode.getFather() == null) return;

		RelicChunk chunk = chunkNode.getChunk();

		Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
		TreePath selectionBackup = treeChunk.getSelectedPath();

		String s = (String) JOptionPane.showInputDialog(frmFileFormatEditor, "New ID:", "Edit ID",
				JOptionPane.PLAIN_MESSAGE, null, null, chunk.get_id());
		if (s == null) return;

		undoMgr.addUndoableGlobalEdit("Edit ID");
		
		chunk.set_id(s);
		treeChunk.forceUpdate();

		treeChunk.loadExpansionState(expansionStateBackup);
		treeChunk.setSelectedPath(selectionBackup);

	}

	public void commandSearchFindID() {
		FindChunkHeader.showDialog(frmFileFormatEditor,treeChunk);
	}

	public void commandSearchFindText() {
		FindChunkText.showDialog(frmFileFormatEditor,treeChunk,chunkEditor);
	}
	
	public void commandSearchFindHex() {
		FindChunkHex.showDialog(frmFileFormatEditor,treeChunk,chunkEditor);
	}
	
	public void commandSearchReplaceText() {
		ReplaceChunkText.showDialog(frmFileFormatEditor,this);
	}
	
	public void commandMenuAbout() {
		AboutDialog.showDialog(frmFileFormatEditor);
	}

	// ---------------------------------------------------------------------------------------- 
	//
	// Tree events listener
	//
	// ---------------------------------------------------------------------------------------- 

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode != null) {
			chunkEditor.setChunk(chunkNode.getChunk());
		}
		else {
			chunkEditor.setChunk(null);
		}
	}

	// ---------------------------------------------------------------------------------------- 
	//
	// Launch application
	//
	// ---------------------------------------------------------------------------------------- 
	
	public static void main(String[] args) {

		
		if (args.length > 0) {
			
			startFile = new File(args[0]);
			
			if (startFile.isFile()) {
				setCurrentDirectory(new File(startFile.getParent()));
			}
			else {
				setCurrentDirectory(startFile);
				startFile = null;
			}
			
		}

		if (currentDirectory==null) {
			
			try {
				currentDirectory = new File(userPreferences.get("directory",null));
			} catch (NullPointerException e) {
			}
			
		}
		
		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				}
				catch (Exception e) {
					e.printStackTrace();
				}

				try {
					FileFormatEditor window = new FileFormatEditor(startFile);
					window.frmFileFormatEditor.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// ---------------------------------------------------------------------------------------- 
	//
	// Fonctions utilitaires
	//
	// ---------------------------------------------------------------------------------------- 
	
	private void invokeAction(Action a) {
		a.actionPerformed(new ActionEvent(this.treeChunk, ActionEvent.ACTION_PERFORMED,
								(String)a.getValue(Action.NAME),
								EventQueue.getMostRecentEventTime(),
								0));
	}

	// Initialise la fen�tre graphique
	private void initialize() {
		
		frmFileFormatEditor = new JFrame();
		frmFileFormatEditor.setTitle(windowTitle);
		frmFileFormatEditor.setBounds(100, 100, 1049, 564);
		frmFileFormatEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmFileFormatEditor.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmFileOpen = new JMenuItem("Open");
		mntmFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmFileOpen.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				commandMenuOpen();
			}
		});
		mnFile.add(mntmFileOpen);

		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmSave.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				commandMenuSave();
			}
		});
		mnFile.add(mntmSave);

		JMenuItem mntmSaveAs = new JMenuItem("Save As...");
		mntmSaveAs.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandMenuSaveAs();
			}
		});
		mnFile.add(mntmSaveAs);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmFileExit = new JMenuItem("Exit");
		mntmFileExit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandMenuExit();
			}
		});
		mnFile.add(mntmFileExit);

		JMenu mnEditChunk = new JMenu("Edit Chunk");
		menuBar.add(mnEditChunk);

		JMenuItem mntmCutChunk = new JMenuItem("Cut");
		mntmCutChunk.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkCut();
			}
		});

		JMenuItem mntmEditId = new JMenuItem("Edit ID");
		mntmEditId.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		mntmEditId.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkEditID();
			}
		});
		
		JMenuItem mntmUndo = new JMenuItem("Undo");
		mntmUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandEditChunkEditUndo();
			}
		});
		mnEditChunk.add(mntmUndo);
		
		JMenuItem mntmRedo = new JMenuItem("Redo");
		mntmRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandEditChunkEditRedo();
			}
		});
		mnEditChunk.add(mntmRedo);
		
		JSeparator separator_6 = new JSeparator();
		mnEditChunk.add(separator_6);
		mnEditChunk.add(mntmEditId);

		JSeparator separator_3 = new JSeparator();
		mnEditChunk.add(separator_3);
		
		JMenuItem mntmDel = new JMenuItem("Del");
		mntmDel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.CTRL_MASK));
		mntmDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkDelete();
			}
		});
		mnEditChunk.add(mntmDel);
		
		JSeparator separator_7 = new JSeparator();
		mnEditChunk.add(separator_7);
		mnEditChunk.add(mntmCutChunk);

		JMenuItem mntmCopyChunk = new JMenuItem("Copy");
		mntmCopyChunk.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkCopy();
			}
		});
		mnEditChunk.add(mntmCopyChunk);

		JMenuItem mntmPasteChunk = new JMenuItem("Paste");
		mntmPasteChunk.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkPaste();
			}
		});
		mnEditChunk.add(mntmPasteChunk);

		JSeparator separator_2 = new JSeparator();
		mnEditChunk.add(separator_2);

		JMenuItem mntmMoveUp = new JMenuItem("Move Up");
		mntmMoveUp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
		mntmMoveUp.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkMoveUp();
			}
		});
		
		JMenuItem mntmExportRaw = new JMenuItem("Export Raw");
		mntmExportRaw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandEditChunkExportRaw();
			}
		});
		mnEditChunk.add(mntmExportRaw);
		
		JMenuItem mntmImportRaw = new JMenuItem("Import Raw");
		mntmImportRaw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkImportRaw();
			}
		});
		mnEditChunk.add(mntmImportRaw);
		
		JSeparator separator_5 = new JSeparator();
		mnEditChunk.add(separator_5);
		mnEditChunk.add(mntmMoveUp);

		JMenuItem mntmMoveDown = new JMenuItem("Move Down");
		mntmMoveDown.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		mntmMoveDown.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandEditChunkMoveDown();
			}
		});
		mnEditChunk.add(mntmMoveDown);
		
		JMenu mnSearch = new JMenu("Search");
		menuBar.add(mnSearch);
		
		JMenuItem mntmFindChunkid = new JMenuItem("Find Name or ID");
		mntmFindChunkid.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmFindChunkid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandSearchFindID();
			}
		});
		mnSearch.add(mntmFindChunkid);
		
		JMenuItem mntmFindText = new JMenuItem("Find Text");
		mntmFindText.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
		mntmFindText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandSearchFindText();
			}
		});
		mnSearch.add(mntmFindText);
		
		JMenuItem mntmFindHex = new JMenuItem("Find Hex");
		mntmFindHex.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
		mntmFindHex.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandSearchFindHex();
			}
		});
		mnSearch.add(mntmFindHex);
		
		JSeparator separator_4 = new JSeparator();
		mnSearch.add(separator_4);
		
		JMenuItem mntmReplaceText = new JMenuItem("Replace Text");
		mntmReplaceText.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		mntmReplaceText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandSearchReplaceText();
			}
		});
		mnSearch.add(mntmReplaceText);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		JMenuItem mntmHelpAbout = new JMenuItem("About");
		mntmHelpAbout.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				commandMenuAbout();
			}
		});
		mnHelp.add(mntmHelpAbout);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.3);
		frmFileFormatEditor.getContentPane().add(splitPane, BorderLayout.CENTER);

		chunkEditor = new ChunkEditor(undoMgr);
		splitPane.setRightComponent(chunkEditor);

		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);

		treeChunk = new TreeChunk(this);
		scrollPane_1.setViewportView(treeChunk);
		treeChunk.setTransferHandler(DEFAULT_TRANSFER_HANDLER);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		frmFileFormatEditor.getContentPane().add(toolBar, BorderLayout.NORTH);
		
		JButton btnOpen = new JButton();
		btnOpen.setToolTipText("Open");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				commandMenuOpen();
			}
		});
		btnOpen.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/document-open-2.png")));
		toolBar.add(btnOpen);
		
		toolBar.addSeparator();
		
		JButton btnSave = new JButton();
		btnSave.setToolTipText("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandMenuSave();
			}
		});
		btnSave.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/document-save-5.png")));
		toolBar.add(btnSave);
		
		toolBar.addSeparator();
		
		JButton btnUndo = new JButton() {
			public String getToolTipText(MouseEvent event) {
				return undoMgr.getUndoPresentationName();
			}
		};
		btnUndo.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/edit-undo-4.png")));
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkEditUndo();
			}
		});
		ToolTipManager.sharedInstance().registerComponent(btnUndo);
		toolBar.add(btnUndo);
		
		JButton btnRedo = new JButton() {
			public String getToolTipText(MouseEvent event) {
				return undoMgr.getRedoPresentationName();
			}
		};
		btnRedo.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/edit-redo-4.png")));
		btnRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkEditRedo();
			}
		});
		ToolTipManager.sharedInstance().registerComponent(btnRedo);
		toolBar.add(btnRedo);
		
		toolBar.addSeparator();
		
		JButton btnCut = new JButton();
		btnCut.setToolTipText("Cut chunk");
		btnCut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkCut();
			}
		});
		btnCut.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/edit-cut-3.png")));
		toolBar.add(btnCut);
		
		JButton btnCopy = new JButton();
		btnCopy.setToolTipText("Copy chunk");
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkCopy();
			}
		});
		btnCopy.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/edit-copy-4.png")));
		toolBar.add(btnCopy);
		
		JButton btnPaste = new JButton();
		btnPaste.setToolTipText("Paste chunk");
		btnPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkPaste();
			}
		});
		btnPaste.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/edit-paste-3.png")));
		toolBar.add(btnPaste);
		
		toolBar.addSeparator();
		
		JButton btnMoveUp = new JButton();
		btnMoveUp.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/go-up-5.png")));
		btnMoveUp.setToolTipText("Move chunk Up");
		btnMoveUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkMoveUp();
			}
		});
		toolBar.add(btnMoveUp);
		
		JButton btnMoveDown = new JButton();
		btnMoveDown.setIcon(new ImageIcon(FileFormatEditor.class.getResource("/icons22/go-down-5.png")));
		btnMoveDown.setToolTipText("Move chunk Down");
		btnMoveDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				commandEditChunkMoveDown();
			}
		});
		toolBar.add(btnMoveDown);
		
		toolBar.addSeparator();
		
		addressPanel = new TreeChunkAdressPanel(treeChunk);
		toolBar.add(addressPanel);

		treeChunk.addTreeSelectionListener(this);
	}

	
}
