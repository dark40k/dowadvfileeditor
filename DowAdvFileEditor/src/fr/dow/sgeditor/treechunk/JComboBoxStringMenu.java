package fr.dow.sgeditor.treechunk;

import java.awt.Component;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class JComboBoxStringMenu extends JComboBox<String> {

	private final JPopupMenu popUp;
	private final JMenuItem copy, cut, paste, selectAll;
	private final JTextField textField;

	public JComboBoxStringMenu() {

		textField = (JTextField) getEditor().getEditorComponent();
		
		popUp = new JPopupMenu();

		paste = new JMenuItem("Paste", new ImageIcon(getClass().getResource("/icons/paste.gif")));
		paste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.paste();
			}
		});
		
		cut = new JMenuItem("Cut", new ImageIcon(getClass().getResource("/icons/cut.gif")));
		cut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.cut();
			}
		});
		
		copy = new JMenuItem("Copy", new ImageIcon(getClass().getResource("/icons/copy.gif")));
		copy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.copy();
			}
		});
		
		selectAll = new JMenuItem("Select All", null);
		selectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.requestFocus();
		        SwingUtilities.invokeLater(new Runnable() {
		            @Override
		            public void run() {
		            	textField.selectAll();
		            }
		        });
			}
		});

		popUp.add(paste);
		popUp.add(cut);
		popUp.add(copy);
		popUp.add(selectAll);

		textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (SwingUtilities.isRightMouseButton(e)) {

					
					if (textField.getSelectedText() == null) {
						copy.setEnabled(false);
						cut.setEnabled(false);
					} else {
						copy.setEnabled(true);
						cut.setEnabled(true);
					}

					if (textField.getText().equals("")) {
						selectAll.setEnabled(false);
					} else {
						selectAll.setEnabled(true);
					}

					Clipboard c = getToolkit().getSystemClipboard();

					Transferable t = c.getContents(this);

					if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {

						String s;

						try {
							s = (String) t.getTransferData(DataFlavor.stringFlavor);
							if (s.equals("")) {
								paste.setEnabled(false);
							} else {
								paste.setEnabled(true);
							}
						} catch (UnsupportedFlavorException ex) {
							ex.printStackTrace();
						} catch (IOException ex) {
							ex.printStackTrace();
						}

					} else {
						paste.setEnabled(false);
					}

					popUp.show((Component) e.getSource(), e.getX(), e.getY());
				} 
			}
		});
	}
}