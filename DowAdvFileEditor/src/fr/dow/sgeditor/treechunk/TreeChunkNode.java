package fr.dow.sgeditor.treechunk;

import java.util.Vector;

import javax.swing.tree.TreePath;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;

public class TreeChunkNode {

	private TreeChunkNode fatherNode;
	
	private String refChunkFileName;
	
	private RelicChunk refChunk;
	private RelicChunkFile refChunkFile;
	
	private Vector<TreeChunkNode> subNodes;

	private TreeChunkNode(RelicChunk chunk, TreeChunkNode father) { 
		
		refChunk=chunk; 
		fatherNode=father;
		
		if (refChunk.get_type().equals(RelicChunkFOLD.type)) {
			subNodes=new Vector<TreeChunkNode>();
			Vector<RelicChunk> subChunks=((RelicChunkFOLD) refChunk).get_subChunks();
			for (int i=0;i<subChunks.size();i++) subNodes.add(new TreeChunkNode(subChunks.get(i),this));
		}
	}
	
	public TreeChunkNode(String baseName, RelicChunkFile chunkFile) { 
		refChunkFile=chunkFile;
		this.refChunkFileName=baseName;
		reloadChunkFile();
	}

	public void reloadChunkFile() {
		subNodes=new Vector<TreeChunkNode>();
		for (int i=0;i<refChunkFile.size();i++) subNodes.add(new TreeChunkNode(refChunkFile.get(i),this));
	}
	
	public void setName(String name) {
		if (refChunk==null) {
			refChunkFileName=name;
			return;
		}
	}
	
	public String toString() { 
		if (refChunk==null) return refChunkFileName;
		return refChunk.get_id()+" "+refChunk.get_name(); 
	}

	public boolean isDATA() { 
		return (subNodes==null);
	}

	public int getChildCount() {
		if (subNodes==null) return 0; 
		return subNodes.size();
	}

	public TreeChunkNode getChildAt(int index) {
		if (subNodes==null) return null;
		return subNodes.get(index);
	}

	public int getIndexOfChild(TreeChunkNode chunkNodeChild) {
		if (subNodes==null) return -1;
		return subNodes.indexOf(chunkNodeChild);
	}

	public RelicChunk getChunk() {
		return refChunk;
	}

	public TreeChunkNode getFather() {
		return fatherNode;
	}

	public void moveUp() {
		
		if (fatherNode==null) return;
		
		int index=fatherNode.subNodes.indexOf(this);
		
		if (index==0) return;
		
		TreeChunkNode previousNode=fatherNode.subNodes.get(index-1);
		fatherNode.subNodes.set(index-1, this);
		fatherNode.subNodes.set(index, previousNode);
		
		if (fatherNode.refChunk==null) {
			fatherNode.refChunkFile.set(index-1, this.getChunk());
			fatherNode.refChunkFile.set(index, previousNode.getChunk());
		} else {
			RelicChunkFOLD fatherChunkFOLD = (RelicChunkFOLD) fatherNode.refChunk;
			fatherChunkFOLD.get_subChunks().set(index-1, this.getChunk());
			fatherChunkFOLD.get_subChunks().set(index, previousNode.getChunk());
		}
		
	}
	
	public void moveDown() {
		
		if (fatherNode==null) return;
		
		int index=fatherNode.subNodes.indexOf(this);
		
		if (index==(fatherNode.subNodes.size()-1)) return;
		
		TreeChunkNode nextNode=fatherNode.subNodes.get(index+1);
		fatherNode.subNodes.set(index+1, this);
		fatherNode.subNodes.set(index, nextNode);
		
		if (fatherNode.refChunk==null) {
			fatherNode.refChunkFile.set(index+1, this.getChunk());
			fatherNode.refChunkFile.set(index, nextNode.getChunk());
		} else {
			RelicChunkFOLD fatherChunkFOLD = (RelicChunkFOLD) fatherNode.refChunk;
			fatherChunkFOLD.get_subChunks().set(index+1, this.getChunk()); 
			fatherChunkFOLD.get_subChunks().set(index, nextNode.getChunk());
		}
		
	}

	public void pasteChunk(RelicChunk chunk) {
		
		if (!refChunk.get_type().equals(RelicChunkFOLD.type)) return;
		
		RelicChunk chunkCopy = (RelicChunk) chunk.clone();
		
		subNodes.add(new TreeChunkNode(chunkCopy,this));
		
		if (refChunkFile!=null) {
			refChunkFile.add(chunkCopy);
			return;
		}
		
		if (!refChunk.get_type().equals(RelicChunkFOLD.type)) return;
		
		((RelicChunkFOLD) refChunk).get_subChunks().add(chunkCopy);			
		
	}

	public void deleteChunk(TreeChunkNode chunkNode) {
		
		if (!subNodes.contains(chunkNode)) return;
		
		subNodes.remove(chunkNode);
		
		if (refChunkFile!=null) {
			refChunkFile.remove(chunkNode.getChunk());
			return;
		}
		
		if (!refChunk.get_type().equals(RelicChunkFOLD.type)) return;
		
		((RelicChunkFOLD) refChunk).get_subChunks().remove(chunkNode.getChunk());			
		
		
	}

	public TreePath getTreePath() {
		if (fatherNode == null) return new TreePath(this);
		return fatherNode.getTreePath().pathByAddingChild(this);
	}

	public TreeChunkNode next() {
		if (subNodes!=null)	
			if (subNodes.size()>0)
				return subNodes.get(0);
		return nextNodeDown();
	}
	
	private TreeChunkNode nextNodeDown() {
		if (fatherNode==null) return null;
		int nextIndex = fatherNode.subNodes.indexOf(this) + 1;
		if (nextIndex == fatherNode.subNodes.size()) return fatherNode.nextNodeDown();
		return fatherNode.subNodes.get(nextIndex);
	}
		
}
