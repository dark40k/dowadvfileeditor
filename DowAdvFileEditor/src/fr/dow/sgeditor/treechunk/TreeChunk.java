package fr.dow.sgeditor.treechunk;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.dow.gamedata.RelicChunkFile;
import fr.dow.sgeditor.FileFormatEditor;

public class TreeChunk extends JPanel {

	private final TreeChunkModel treeChunkModel=new TreeChunkModel();
	
	private final JTree tree;

	private FileFormatEditor fileFormatEditor;
	
	public TreeChunk(FileFormatEditor fileFormatEditor) {
		
		this.fileFormatEditor = fileFormatEditor;
		
		setLayout(new BorderLayout(0, 0));
		tree = new JTree();
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setExpandsSelectedPaths(true);
		add(tree, BorderLayout.CENTER);
		setChunkFile("<empty>", null);
	}

	
	public FileFormatEditor getFileFormatEditor() {
		return fileFormatEditor;
	}
	
	public JTree getTree() {
		return tree;
	}
	
	public TreeChunkModel getTreeModel() {
		return treeChunkModel;
	}
	
	
	public void setChunkFile(String baseName, RelicChunkFile chunkFile) {

		if (chunkFile==null) {
			tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode(baseName)));
			tree.setEnabled(false);
			return;
		}
		
		treeChunkModel.setNewRootChunkFile(baseName, chunkFile);
		tree.setModel(treeChunkModel);
		tree.getSelectionModel().clearSelection();
		tree.setEnabled(true);

	}

	public void setBaseName(String baseName) {
		treeChunkModel.setBaseName(baseName);
	}
	
	
	public void addTreeSelectionListener(TreeSelectionListener listener) {
		tree.addTreeSelectionListener(listener);
	}

	public TreeChunkNode getLastSelectedNode() {
		Object lastComponent = tree.getLastSelectedPathComponent();
		if (lastComponent == null) return null;
		return (TreeChunkNode) lastComponent;
	}
	
	public TreePath getSelectedPath() {
		if (tree.getSelectionCount()==0) return null;
		return tree.getSelectionPath();
	}
	
	public void setSelectedPath(TreePath path) {
		tree.setSelectionPath(path);
	}
	
	public void forceUpdate() {
		treeChunkModel.forceUpdate();
	}
	
	public void forceCompleteUpdate() {
		treeChunkModel.forceCompleteUpdate();
	}
	
     /**
     * Save the expansion state of a tree.
     *
     * @param tree
     * @return expanded tree path as Enumeration
     */
    public Enumeration<TreePath> saveExpansionState() {
        return tree.getExpandedDescendants(new TreePath(tree.getModel().getRoot()));
    }


    /**
     * Restore the expansion state of a JTree.
     *
     * @param tree
     * @param enumeration an Enumeration of expansion state. You can get it using {@link #saveExpansionState(javax.swing.JTree)}.
     */

    public void loadExpansionState(Enumeration<TreePath> enumeration) {
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                TreePath treePath = enumeration.nextElement();
                tree.expandPath(treePath);
            }
        }
    }

    public ArrayList<Integer> saveRowExpansionState() {
    	ArrayList<Integer> expList = new ArrayList<Integer>();
        for (int row=0; row<tree.getRowCount(); row++) {
        	if (tree.isExpanded(row)) expList.add(row);
        }
        return expList;
    }

    public void loadRowExpansionState(ArrayList<Integer> expList) {
    	for (int i=0; i<expList.size(); i++){
    		Integer row = expList.get(i);
    		tree.expandRow(row);
    	}
    }
	
}
