package fr.dow.sgeditor.treechunk;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayDeque;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

public class TreeChunkAdressPanel extends JPanel implements TreeModelListener, TreeSelectionListener {

	private JComboBox<String> addressComboBox;
	
	private final TreeChunk treeChunk;
	private final TreeChunkModel treeModel;

	private final LinkedHashMap<String,TreeChunkNode> listNodes= new LinkedHashMap<String,TreeChunkNode>();
	private final LinkedHashMap<TreeChunkNode,String> listPaths= new LinkedHashMap<TreeChunkNode,String>();
		
	public TreeChunkAdressPanel(TreeChunk treeChunk) {
		
		this.treeChunk=treeChunk;
		this.treeModel = treeChunk.getTreeModel();
		
		initGUI();

		// see http://www.orbital-computer.de/JComboBox/#usage
		//AutoCompletion.enable(addressComboBox);
		addressComboBox.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (addressComboBox.getSelectedIndex()<0) return;
				TreeChunkNode node=listNodes.get(addressComboBox.getSelectedItem());
				
				TreeChunkAdressPanel.this.treeChunk.setSelectedPath(node.getTreePath());
			}
		});

		addressComboBox.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() { 
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() != 38 && e.getKeyCode() != 40 && e.getKeyCode() != 10) {
					
					String a = addressComboBox.getEditor().getItem().toString();
					
					addressComboBox.removeAllItems();
					int st = 0;
					
					for ( Entry<String, TreeChunkNode> entry : listNodes.entrySet()) {
						if (entry.getKey().startsWith(a)) {
							addressComboBox.addItem(entry.getKey());
							st++;
						}
					}

					addressComboBox.getEditor().setItem(new String(a));
					JTextField textField = (JTextField)e.getSource();
					textField.setCaretPosition( textField.getDocument().getLength() );
					
					addressComboBox.hidePopup();
					if (st != 0) {
						addressComboBox.showPopup();
					}
				}
			}
		});
		
		updateList();
		
		this.treeChunk.addTreeSelectionListener(this);
		treeModel.addTreeModelListener(this);
		
	}
		
	//
	// Mise � jours des choix de la combobox
	//
	private void updateList() {
		
		listPaths.clear();
		listNodes.clear();
		addressComboBox.removeAllItems();
		
		ArrayDeque<TreeChunkNode> stack = new ArrayDeque<TreeChunkNode>();
		if (treeModel.getRoot() != null) stack.push((TreeChunkNode) treeModel.getRoot());
		
		while (!stack.isEmpty()) {
			TreeChunkNode node = stack.pollFirst();
			
			for (int i=0;i<node.getChildCount();i++) {
				stack.addLast(node.getChildAt(i));
			}
			
			String newAddress = (node.getFather()==null)?"":listPaths.get(node.getFather())+"/"+node.getChunk().get_id();
			
			if (listNodes.containsKey(newAddress)) {
				int i=1;
				while(listNodes.containsKey(newAddress+"("+i+")")) i++;
				newAddress=newAddress+"("+i+")";
			}
			
			listNodes.put(newAddress, node);
			listPaths.put(node, newAddress);
			
			addressComboBox.addItem(newAddress);
		}
		
	}
	
	public static String calcAdressName(TreePath treePath) {
		String pathString=null;
		for (Object path : treePath.getPath()) {
			if (pathString==null) {
				pathString="";
				continue;
			}
			pathString+="/"+path.toString();
		}
		return pathString;
	}
	
	//
	// Initialisation contenu graphique
	//
	
	private void initGUI() {
		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWeights = new double[]{1.0};
		gbl_panel.rowWeights = new double[]{0.0};
		setLayout(gbl_panel);
		
		addressComboBox = new JComboBoxStringMenu();
		addressComboBox.setEditable(true);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		add(addressComboBox, gbc_comboBox);

	}
	
	//
	// Gestion des evenements du modele
	//
	
	@Override
	public void treeNodesChanged(TreeModelEvent e) {
		updateList();
	}

	@Override
	public void treeNodesInserted(TreeModelEvent e) {
		updateList();
	}

	@Override
	public void treeNodesRemoved(TreeModelEvent e) {
		updateList();
	}

	@Override
	public void treeStructureChanged(TreeModelEvent e) {
		updateList();
	}

	//
	// Gestion des evenements de l'arbre
	//
	
	@Override
	public void valueChanged(TreeSelectionEvent arg0) {
		TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
		if (chunkNode != null) {
//			addressComboBox.setSelectedIndex(-1);
			for (Entry<String, TreeChunkNode> entry : listNodes.entrySet())
				if (entry.getValue()==chunkNode)
					addressComboBox.getEditor().setItem(entry.getKey());
		}
	}
	
}
