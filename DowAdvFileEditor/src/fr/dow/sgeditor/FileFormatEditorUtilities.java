package fr.dow.sgeditor;

import java.util.Arrays;

import fr.dow.gamedata.RelicChunkDATA;


public class FileFormatEditorUtilities {

	public static int findData(RelicChunkDATA refChunk, byte[] searchedHex) {
		if (refChunk==null) return -1;
		return findData(refChunk.dataArray(), searchedHex);
	}
	
	public static int findData(RelicChunkDATA refChunk, byte[] searchedHex, int start) {
		if (refChunk==null) return -1;
		return findData(refChunk.dataArray(), searchedHex, start, refChunk.dataArray().length-1);
	}

	public static int findData(RelicChunkDATA refChunk, byte[] searchedHex, int start, int end) {
		if (refChunk==null) return -1;
		return findData(refChunk.dataArray(), searchedHex, start, end);
	}
	
    public static int findData(byte[] data, byte[] pattern, int start, int end) {
    	int index=findData(Arrays.copyOfRange(data, start, end+1), pattern);
    	if (index<0) return -1;
    	return index+start;
    }	
	
    
    public static byte[] replaceData(byte[] data, int start, int end, byte[] replacePattern) {
    	
    	byte[] newData = new byte[data.length + start - end + replacePattern.length ];
    	
    	for (int i=0;i<start;i++) newData[i]=data[i];
    	
    	for (int i=0;i<replacePattern.length;i++) newData[start+i]=replacePattern[i];
    	
    	int i1 = start - end + replacePattern.length;
    	for (int i=end; i<data.length; i++) newData[i+i1]=data[i];
    	
    	return newData;
    }
	
    
    /**
     * Search the data byte array for the first occurrence
     * of the byte array pattern.
     */
    public static int findData(byte[] data, byte[] pattern) {
        int[] failure = computeFailure(pattern);

        int j = 0;

        for (int i = 0; i < data.length; i++) {
            while (j > 0 && pattern[j] != data[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == data[i]) {
                j++;
            }
            if (j == pattern.length) {
                return i - pattern.length + 1;
            }
        }
        return -1;
    }
    
    /**
     * Computes the failure function using a boot-strapping process,
     * where the pattern is matched against itself.
     */
    private static int[] computeFailure(byte[] pattern) {
        int[] failure = new int[pattern.length];

        int j = 0;
        for (int i = 1; i < pattern.length; i++) {
            while (j>0 && pattern[j] != pattern[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == pattern[i]) {
                j++;
            }
            failure[i] = j;
        }

        return failure;
    }

	
	
}
