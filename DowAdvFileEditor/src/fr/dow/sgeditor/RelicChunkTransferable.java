package fr.dow.sgeditor;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import fr.dow.gamedata.RelicChunk;

/**
 * A <code>Transferable</code> that transfers an array of bytes.
 *
 * @author Robert Futrell
 * @version 1.0
 */

class RelicChunkTransferable implements Transferable, Serializable {

	public static final DataFlavor FLAVOR = new DataFlavor(RelicChunkTransferable.class, "FFE RelicChunk transfert");
	
	private static final DataFlavor[] FLAVORS = {
		FLAVOR,
	};

	private String flattenedChunk;

	public RelicChunkTransferable(RelicChunk node) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			node.write(bos);
		}
		catch (IOException e1) {
			System.err.println("Error: could not create transferable");
			e1.printStackTrace();
		}
		
		flattenedChunk=Base64.encode(bos.toByteArray());
		
	}

	public RelicChunk getChunk() {
		
		if (flattenedChunk==null) return null;
		
		RelicChunk chunk = null;
		byte[] bytes;
		
		try {
			bytes = Base64.decode(flattenedChunk);
			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			chunk = RelicChunk.loadChunk(bis);
		}
		catch (Base64DecodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return chunk;
	}
	
	
	/**
	 * Returns the data being transferred in a format specified by the
	 * <code>DataFlavor</code>.
	 *
	 * @param flavor Dictates in what format the data should be returned.
	 * @throws UnsupportedFlavorException If the specified flavor is not
	 *         supported.
	 * @throws IOException If an IO error occurs.
	 * @see DataFlavor#getRepresentationClass()
	 */
	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		if (flavor.equals(FLAVORS[0])) {
			return this;
		}
	    throw new UnsupportedFlavorException(flavor);
	}


	/**
	 * Returns an array of DataFlavor objects indicating the flavors the data 
	 * can be provided in.  The array is ordered according to preference for 
	 * providing the data (from most richly descriptive to least descriptive).
	 *
	 * @return An array of data flavors in which this data can be transferred.
	 */
	public DataFlavor[] getTransferDataFlavors() {
		return (DataFlavor[])FLAVORS.clone();
	}


	/**
	 * Returns whether a data flavor is supported.
	 *
	 * @param flavor The flavor to check.
	 * @return Whether the specified flavor is supported.
	 */
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		for (int i=0; i<FLAVORS.length; i++) {
			if (flavor.equals(FLAVORS[i])) {
				return true;
			}
		}
		return false;
	}


}