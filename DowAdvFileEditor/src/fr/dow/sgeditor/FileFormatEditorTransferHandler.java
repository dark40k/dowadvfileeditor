package fr.dow.sgeditor;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.tree.TreePath;

import fr.dow.gamedata.RelicChunk;
import fr.dow.sgeditor.FileFormatEditorUndoMgr.UndoableNodeDatabaseEdit;
import fr.dow.sgeditor.treechunk.TreeChunk;
import fr.dow.sgeditor.treechunk.TreeChunkNode;

public class FileFormatEditorTransferHandler  extends TransferHandler {
	
	public final static String HEADER = "FFENODE=";
	
	private static final long serialVersionUID = 1L;

	public boolean canImport(JComponent comp, DataFlavor[] flavors) {
		TreeChunk editor = (TreeChunk)comp;
		if (!editor.isEnabled()) {
			return false;
		}
		return getImportFlavor(flavors, editor)!=null;
	}


	protected Transferable createTransferable(JComponent c) {
		TreeChunk e = (TreeChunk)c;
		TreeChunkNode chunkNode = e.getLastSelectedNode();
		return new RelicChunkTransferable(chunkNode.getChunk());
	}


	protected void exportDone(JComponent c, Transferable data, int action){
		if (action==MOVE) {
			
			TreeChunk treeChunk = (TreeChunk)c;
			TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();

			TreeChunkNode father = chunkNode.getFather();
			if (father == null) return;

			treeChunk.getFileFormatEditor().getUndoMgr().addUndoableGlobalEdit("Cut chunk");
			
			Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
			
			father.deleteChunk(chunkNode);
			treeChunk.forceUpdate();

			treeChunk.loadExpansionState(expansionStateBackup);
		
		}
	}


	private DataFlavor getImportFlavor(DataFlavor[] flavors, TreeChunk e) {
		for (int i=0; i<flavors.length; i++) {
			if (flavors[i].equals(RelicChunkTransferable.FLAVOR)) {
				return flavors[i];
			}
		}
		return null;
	}


	/**
	 * Returns what operations can be done on a hex editor (copy and move, or
	 * just copy).
	 *
	 * @param c The <code>HexEditor</code>.
	 * @return The permitted operations.
	 */
	public int getSourceActions(JComponent c) {
		TreeChunk treeChunk = (TreeChunk) c;
		return treeChunk.isEnabled() ? COPY_OR_MOVE : COPY;
	}


	/**
	 * Imports data into a hex editor component.
	 *
	 * @param c The <code>HexEditor</code> component.
	 * @param t The data to be imported.
	 * @return Whether the data was successfully imported.
	 */
	public boolean importData(JComponent c, Transferable t) {

		TreeChunk treeChunk = (TreeChunk) c;
		boolean imported = false;
		
		TreeChunkNode fatherNode = treeChunk.getLastSelectedNode();

		UndoableNodeDatabaseEdit newEdit = treeChunk.getFileFormatEditor().getUndoMgr().createUndoableGlobalEdit("Paste chunk");

		DataFlavor flavor = getImportFlavor(t.getTransferDataFlavors(), treeChunk);
		if (flavor!=null) {
			try {
				
				Object data = t.getTransferData(flavor);
				
				if (flavor.equals(RelicChunkTransferable.FLAVOR)) {
					
					RelicChunkTransferable nodetrans = (RelicChunkTransferable) data;
					
					RelicChunk chunk = nodetrans.getChunk();
					
					Enumeration<TreePath> expansionStateBackup = treeChunk.saveExpansionState();
					TreePath selectionBackup = treeChunk.getSelectedPath();
					
					fatherNode.pasteChunk(chunk);
					
					treeChunk.getFileFormatEditor().getUndoMgr().addEdit(newEdit);
					
					treeChunk.forceUpdate();

					treeChunk.loadExpansionState(expansionStateBackup);
					treeChunk.setSelectedPath(selectionBackup);
					
				}
			} catch (UnsupportedFlavorException ufe) {
				ufe.printStackTrace(); // Never happens.
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} 
		}

		return imported;

	}


}