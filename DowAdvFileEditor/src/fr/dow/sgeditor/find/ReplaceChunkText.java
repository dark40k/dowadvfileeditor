package fr.dow.sgeditor.find;

import java.awt.Window;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.sgeditor.FileFormatEditor;
import fr.dow.sgeditor.FileFormatEditorUtilities;
import fr.dow.sgeditor.treechunk.TreeChunkNode;

public class ReplaceChunkText extends ReplaceChunkTextBase {

	private FileFormatEditor fileFormatEditor;
	
	/**
	 * Launch the application.
	 * 
	 * @param chunkEditor
	 */
	public static void showDialog(Window owner, FileFormatEditor fileFormatEditor) {
		try {
			ReplaceChunkText dialog = new ReplaceChunkText(owner, fileFormatEditor);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ReplaceChunkText(Window owner, FileFormatEditor fileFormatEditor) {
		super(owner);
		this.fileFormatEditor = fileFormatEditor;
	}

	@Override
	protected void doReplace() {

		byte[] searchPattern = getTextSearchString().getText().getBytes();
		byte[] replacePattern = getTextReplaceString().getText().getBytes();
		boolean autoResizeString = getChckbxAutoUpdateString().isSelected();
		
		fileFormatEditor.getUndoMgr().addUndoableGlobalEdit("Replace");
		
		if (getRdbtnSelection().isSelected()) {
			doReplaceStringInSelection(searchPattern,replacePattern,autoResizeString);
			return;
		}

		TreeChunkNode chunkNode;

		if (getRdbtnChunk().isSelected()) 
			chunkNode = fileFormatEditor.getTreeChunk().getLastSelectedNode();
		else 
			chunkNode = (TreeChunkNode) fileFormatEditor.getTreeChunk().getTreeModel().getRoot();		
		
		if (chunkNode == null) return;
		
		Vector<RelicChunkDATA> chunks = getSubChunksDATA(chunkNode);

		for (int i=0; i<chunks.size();i++) {
			
			if (getRdbtnAllFileAndName().isSelected()) {
				String nameIn=chunks.get(i).get_name();
				String nameOut=nameIn.replaceAll(getTextSearchString().getText(), getTextReplaceString().getText());
				if ((nameIn !=null) && (!nameIn.equals(nameOut))) 
						chunks.get(i).set_name(nameOut);
			}
			
			doReplaceStringInChunkNode(chunks.get(i),searchPattern,replacePattern,autoResizeString);
			
		}

		fileFormatEditor.getChunkEditor().updateChunkData();

	}
	
	private Vector<RelicChunkDATA> getSubChunksDATA(TreeChunkNode father) {
		Vector<RelicChunkDATA> chunks = new Vector<RelicChunkDATA>();
		addSubChunksDATA( father, chunks);
		return chunks;
	}
	
	private void addSubChunksDATA( TreeChunkNode father, Vector<RelicChunkDATA> chunks) {
		if (father.isDATA()) {
			chunks.add((RelicChunkDATA) father.getChunk());
			return;
		}
		for (int i=0;i<father.getChildCount();i++) addSubChunksDATA(father.getChildAt(i),chunks);
	}
	
	private void doReplaceStringInSelection(byte[] searchPattern, byte[] newPattern, boolean autoResizeString) {

		RelicChunk curChunk = fileFormatEditor.getChunkEditor().getChunk();
		if (curChunk == null) return;
		if (!curChunk.get_type().equals(RelicChunkDATA.type)) return;

		byte [] newSelection = fileFormatEditor.getChunkEditor().getSelectionCopy();
		newSelection = replaceStringInByteArray(newSelection,searchPattern,newPattern,autoResizeString);
		fileFormatEditor.getChunkEditor().replaceSelection(newSelection);
		
	}

	private void doReplaceStringInChunkNode(RelicChunkDATA chunk, byte[] searchPattern, byte[] replacePattern, boolean autoResizeString) {
		if (chunk == null) return;
		byte [] newArray = chunk.dataArray();
		newArray = replaceStringInByteArray(newArray,searchPattern,replacePattern,autoResizeString);
		chunk.dataWrapArray(newArray);
		chunk.updateSize();
	}

	private byte [] replaceStringInByteArray(byte [] source, byte[] searchPattern, byte[] newPattern, boolean autoResizeString) {
		
		byte[] byteArray = source;
		
		int index=0;
		while (true) {
			index = FileFormatEditorUtilities.findData(byteArray, searchPattern,index,byteArray.length-1);
	
			if (index < 0) break;
			
			byteArray=FileFormatEditorUtilities.replaceData(byteArray, index, index+searchPattern.length, newPattern);
			
			if (autoResizeString) {
				
				// scan upward for 1st 0x00
				int sizeIndex=index;
				while (sizeIndex>=0) { if (byteArray[sizeIndex]==0) break; sizeIndex--; }
				
				// move 3 bytes up to get start of size block
				sizeIndex=sizeIndex-3;
				if (sizeIndex<0) {
					JOptionPane.showMessageDialog(this, "Initial 4 bytes length not found.", "Selection Error",JOptionPane.ERROR_MESSAGE);
					return source;
				}
				
				// load block size
				int size = byteArray[sizeIndex + 3] << 24 | (byteArray[sizeIndex + 2] & 0xff) << 16
						| (byteArray[sizeIndex + 1] & 0xff) << 8 | (byteArray[sizeIndex + 0] & 0xff);

				// update size
				size=size - searchPattern.length +  newPattern.length;
				
				// write new size
				byteArray[sizeIndex+0]=(byte) ((size & 0x000000FF) >> 0);
				byteArray[sizeIndex+1]=(byte) ((size & 0x0000FF00) >> 8);
				byteArray[sizeIndex+2]=(byte) ((size & 0x00FF0000) >> 16);
				byteArray[sizeIndex+3]=(byte) ((size & 0xFF000000) >> 24);
				
			}
			
			index=index+newPattern.length;
		}
		return byteArray;
	}
	
	@Override
	protected void doClose() {
		this.setVisible(false);
	}


	@Override
	protected void doSearchPasteSel() {
		getTextSearchString().setText(fileFormatEditor.getChunkEditor().getSelectionAsString());
	}

	@Override
	protected void doReplacePasteSel() {
		getTextReplaceString().setText(fileFormatEditor.getChunkEditor().getSelectionAsString());
	}

}
