package fr.dow.sgeditor.find;

import java.awt.Window;

import javax.swing.JDialog;

import fr.dow.sgeditor.treechunk.TreeChunk;
import fr.dow.sgeditor.treechunk.TreeChunkNode;


public class FindChunkHeader extends FindChunkHeaderBase {

	TreeChunk treeChunk;
	
	/**
	 * Launch the application.
	 * @param fileFormatEditor 
	 */
	public static void showDialog(Window owner, TreeChunk treeChunk) {
		try {
			FindChunkHeader dialog = new FindChunkHeader(owner,treeChunk);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FindChunkHeader(Window owner, TreeChunk treeChunk) {
		super(owner);
		this.treeChunk=treeChunk;
	}
	
	@Override
	void doFindNext() {
		if (this.getRdbtnChunkID().isSelected()) {
			selectNextID(this.getTextFieldSearchString().getText(),this.getChckbxIgnoreCase().isSelected());
		} else if (this.getRdbtnChunkName().isSelected()) {
			selectNextName(this.getTextFieldSearchString().getText(),this.getChckbxIgnoreCase().isSelected());
		}
		
	}

	@Override
	void doClose() {
		this.setVisible(false);
	}

	public void selectNextID(String text, boolean ignoreCase) {
		if (!treeChunk.isEnabled()) return;
		
		TreeChunkNode startNode = treeChunk.getLastSelectedNode();
		if (startNode!=null) startNode=startNode.next();
		else startNode=((TreeChunkNode) treeChunk.getTreeModel().getRoot()).next();
		
		TreeChunkNode nextNode=treeChunk.getTreeModel().findNextNodeID(text,startNode,ignoreCase);
		if (nextNode==null) return;
		
		treeChunk.getTree().scrollPathToVisible(nextNode.getTreePath());
		treeChunk.getTree().setSelectionPath( nextNode.getTreePath() );
		
	}

	public void selectNextName(String text, boolean ignoreCase) {
		if (!treeChunk.isEnabled()) return;
		
		TreeChunkNode startNode = treeChunk.getLastSelectedNode();
		if (startNode!=null) startNode=startNode.next();
		else startNode=((TreeChunkNode) treeChunk.getTreeModel().getRoot()).next();

		TreeChunkNode nextNode=treeChunk.getTreeModel().findNextNodeName(text,startNode,ignoreCase);
		if (nextNode==null) return;
		
		treeChunk.getTree().scrollPathToVisible(nextNode.getTreePath());
		treeChunk.getTree().setSelectionPath(nextNode.getTreePath() );
	}
	
	
}
