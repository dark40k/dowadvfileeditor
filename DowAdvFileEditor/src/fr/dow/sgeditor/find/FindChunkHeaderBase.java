package fr.dow.sgeditor.find;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;


public abstract class FindChunkHeaderBase extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldSearchString;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnChunkID;
	private JRadioButton rdbtnChunkName;
	private JCheckBox chckbxIgnoreCase;

	/**
	 * Create the dialog.
	 */
	public FindChunkHeaderBase(Window owner) {
		super(owner);
		setTitle("Find Chunk Header");
		setBounds(100, 100, 349, 263);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{98, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Character search sequence", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.anchor = GridBagConstraints.NORTH;
			gbc_panel.fill = GridBagConstraints.HORIZONTAL;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 0;
			contentPanel.add(panel, gbc_panel);
			panel.setLayout(new BorderLayout(0, 0));
			{
				textFieldSearchString = new JTextField();
				panel.add(textFieldSearchString);
				textFieldSearchString.setColumns(10);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search scope", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 1;
			contentPanel.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				rdbtnChunkID = new JRadioButton("Chunk ID");
				rdbtnChunkID.setSelected(true);
				buttonGroup.add(rdbtnChunkID);
				GridBagConstraints gbc_rdbtnChunkID = new GridBagConstraints();
				gbc_rdbtnChunkID.anchor = GridBagConstraints.WEST;
				gbc_rdbtnChunkID.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnChunkID.gridx = 0;
				gbc_rdbtnChunkID.gridy = 0;
				panel.add(rdbtnChunkID, gbc_rdbtnChunkID);
			}
			{
				rdbtnChunkName = new JRadioButton("Chunk Name");
				buttonGroup.add(rdbtnChunkName);
				GridBagConstraints gbc_rdbtnChunkName = new GridBagConstraints();
				gbc_rdbtnChunkName.anchor = GridBagConstraints.WEST;
				gbc_rdbtnChunkName.gridx = 0;
				gbc_rdbtnChunkName.gridy = 1;
				panel.add(rdbtnChunkName, gbc_rdbtnChunkName);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 2;
			contentPanel.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{83, 0};
			gbl_panel.rowHeights = new int[]{23, 0};
			gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				chckbxIgnoreCase = new JCheckBox("Ignore case");
				chckbxIgnoreCase.setSelected(true);
				GridBagConstraints gbc_chckbxIgnoreCase = new GridBagConstraints();
				gbc_chckbxIgnoreCase.insets = new Insets(0, 0, 5, 0);
				gbc_chckbxIgnoreCase.anchor = GridBagConstraints.NORTHWEST;
				gbc_chckbxIgnoreCase.gridx = 0;
				gbc_chckbxIgnoreCase.gridy = 0;
				panel.add(chckbxIgnoreCase, gbc_chckbxIgnoreCase);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{79, 0, 59, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnFindNext = new JButton("Find Next");
				btnFindNext.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doFindNext();
					}
				});
				btnFindNext.setActionCommand("Cancel");
				GridBagConstraints gbc_btnFindNext = new GridBagConstraints();
				gbc_btnFindNext.anchor = GridBagConstraints.NORTHWEST;
				gbc_btnFindNext.insets = new Insets(5, 5, 5, 5);
				gbc_btnFindNext.gridx = 0;
				gbc_btnFindNext.gridy = 0;
				buttonPane.add(btnFindNext, gbc_btnFindNext);
			}
			{
				JButton closeButton = new JButton("Close");
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						doClose();
					}
				});
				closeButton.setActionCommand("Cancel");
				GridBagConstraints gbc_closeButton = new GridBagConstraints();
				gbc_closeButton.insets = new Insets(5, 5, 5, 5);
				gbc_closeButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_closeButton.gridx = 2;
				gbc_closeButton.gridy = 0;
				buttonPane.add(closeButton, gbc_closeButton);
			}
		}
	}

	abstract void doFindNext();
	abstract void doClose();

	protected JRadioButton getRdbtnChunkID() {
		return rdbtnChunkID;
	}
	protected JRadioButton getRdbtnChunkName() {
		return rdbtnChunkName;
	}
	protected JTextField getTextFieldSearchString() {
		return textFieldSearchString;
	}
	protected JCheckBox getChckbxIgnoreCase() {
		return chckbxIgnoreCase;
	}
}
