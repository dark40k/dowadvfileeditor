package fr.dow.sgeditor.find;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public abstract class FindChunkHexBase extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textSearchedString;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnSelection;
	private JRadioButton rdbtnCompleteFile;

	/**
	 * Create the dialog.
	 */
	public FindChunkHexBase(Window owner) {
		super(owner);
		setTitle("Find Hex");
		setBounds(100, 100, 450, 201);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{98, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search Hexadecimal sequence", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.anchor = GridBagConstraints.NORTH;
			gbc_panel.fill = GridBagConstraints.HORIZONTAL;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 0;
			contentPanel.add(panel, gbc_panel);
			panel.setLayout(new BorderLayout(0, 0));
			{
				textSearchedString = new JTextField();
				panel.add(textSearchedString);
				textSearchedString.setColumns(10);
			}
			{
				JButton btnPaste = new JButton("Paste Sel.");
				btnPaste.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doPasteSel();
					}
				});
				panel.add(btnPaste, BorderLayout.EAST);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 1;
			contentPanel.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				rdbtnSelection = new JRadioButton("Selection");
				buttonGroup.add(rdbtnSelection);
				GridBagConstraints gbc_rdbtnSelection = new GridBagConstraints();
				gbc_rdbtnSelection.anchor = GridBagConstraints.WEST;
				gbc_rdbtnSelection.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnSelection.gridx = 0;
				gbc_rdbtnSelection.gridy = 0;
				panel.add(rdbtnSelection, gbc_rdbtnSelection);
			}
			{
				rdbtnCompleteFile = new JRadioButton("Forward search (including sub-chunks)");
				rdbtnCompleteFile.setSelected(true);
				buttonGroup.add(rdbtnCompleteFile);
				GridBagConstraints gbc_rdbtnCompleteFile = new GridBagConstraints();
				gbc_rdbtnCompleteFile.anchor = GridBagConstraints.WEST;
				gbc_rdbtnCompleteFile.gridx = 0;
				gbc_rdbtnCompleteFile.gridy = 1;
				panel.add(rdbtnCompleteFile, gbc_rdbtnCompleteFile);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{79, 0, 59, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnFindNext = new JButton("Find Next");
				btnFindNext.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doFindNext();
					}
				});
				btnFindNext.setActionCommand("Cancel");
				GridBagConstraints gbc_btnFindNext = new GridBagConstraints();
				gbc_btnFindNext.anchor = GridBagConstraints.NORTHWEST;
				gbc_btnFindNext.insets = new Insets(5, 5, 5, 5);
				gbc_btnFindNext.gridx = 0;
				gbc_btnFindNext.gridy = 0;
				buttonPane.add(btnFindNext, gbc_btnFindNext);
			}
			{
				JButton closeButton = new JButton("Close");
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						doClose();
					}
				});
				closeButton.setActionCommand("Cancel");
				GridBagConstraints gbc_closeButton = new GridBagConstraints();
				gbc_closeButton.insets = new Insets(5, 5, 5, 5);
				gbc_closeButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_closeButton.gridx = 2;
				gbc_closeButton.gridy = 0;
				buttonPane.add(closeButton, gbc_closeButton);
			}
		}
	}

	protected abstract void doFindNext();
	
	protected abstract void doClose();
	
	protected abstract void doPasteSel();
	
	protected JRadioButton getRdbtnSelection() {
		return rdbtnSelection;
	}
	
	protected JRadioButton getRdbtnCompleteFile() {
		return rdbtnCompleteFile;
	}
	
	protected JTextField getTextSearchedString() {
		return textSearchedString;
	}
	
}
