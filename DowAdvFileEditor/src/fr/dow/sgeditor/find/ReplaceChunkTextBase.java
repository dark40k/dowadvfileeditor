package fr.dow.sgeditor.find;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;


public abstract class ReplaceChunkTextBase extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textSearchedString;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnSelection;
	private JRadioButton rdbtnChunk;
	private JTextField textField;
	private JButton button;
	private JRadioButton rdbtnAllFile;
	private JCheckBox chckbxAutoUpdateString;
	private JRadioButton rdbtnAllFileAndName;

	/**
	 * Create the dialog.
	 */
	public ReplaceChunkTextBase(Window owner) {
		super(owner);
		setTitle("Replace String");
		setBounds(100, 100, 450, 367);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{98, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search string", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.anchor = GridBagConstraints.NORTH;
			gbc_panel.fill = GridBagConstraints.HORIZONTAL;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 0;
			contentPanel.add(panel, gbc_panel);
			panel.setLayout(new BorderLayout(0, 0));
			{
				textSearchedString = new JTextField();
				panel.add(textSearchedString);
				textSearchedString.setColumns(10);
			}
			{
				JButton btnPaste = new JButton("Paste Sel.");
				btnPaste.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doSearchPasteSel();
					}
				});
				panel.add(btnPaste, BorderLayout.EAST);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Replace String", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 1;
			contentPanel.add(panel, gbc_panel);
			panel.setLayout(new BorderLayout(0, 0));
			{
				textField = new JTextField();
				textField.setColumns(10);
				panel.add(textField);
			}
			{
				button = new JButton("Paste Sel.");
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doReplacePasteSel();
					}
				});
				panel.add(button, BorderLayout.EAST);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 2;
			contentPanel.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				rdbtnSelection = new JRadioButton("Selection");
				buttonGroup.add(rdbtnSelection);
				GridBagConstraints gbc_rdbtnSelection = new GridBagConstraints();
				gbc_rdbtnSelection.anchor = GridBagConstraints.WEST;
				gbc_rdbtnSelection.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnSelection.gridx = 0;
				gbc_rdbtnSelection.gridy = 0;
				panel.add(rdbtnSelection, gbc_rdbtnSelection);
			}
			{
				rdbtnChunk = new JRadioButton("Selected chunk (and sub-chunks)");
				rdbtnChunk.setSelected(true);
				buttonGroup.add(rdbtnChunk);
				GridBagConstraints gbc_rdbtnChunk = new GridBagConstraints();
				gbc_rdbtnChunk.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnChunk.anchor = GridBagConstraints.WEST;
				gbc_rdbtnChunk.gridx = 0;
				gbc_rdbtnChunk.gridy = 1;
				panel.add(rdbtnChunk, gbc_rdbtnChunk);
			}
			{
				rdbtnAllFile = new JRadioButton("All file");
				buttonGroup.add(rdbtnAllFile);
				GridBagConstraints gbc_rdbtnAllFile = new GridBagConstraints();
				gbc_rdbtnAllFile.insets = new Insets(0, 0, 5, 0);
				gbc_rdbtnAllFile.anchor = GridBagConstraints.WEST;
				gbc_rdbtnAllFile.gridx = 0;
				gbc_rdbtnAllFile.gridy = 2;
				panel.add(rdbtnAllFile, gbc_rdbtnAllFile);
			}
			{
				rdbtnAllFileAndName = new JRadioButton("All file + Name");
				buttonGroup.add(rdbtnAllFileAndName);
				GridBagConstraints gbc_rdbtnAllFileAndName = new GridBagConstraints();
				gbc_rdbtnAllFileAndName.anchor = GridBagConstraints.WEST;
				gbc_rdbtnAllFileAndName.gridx = 0;
				gbc_rdbtnAllFileAndName.gridy = 3;
				panel.add(rdbtnAllFileAndName, gbc_rdbtnAllFileAndName);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 3;
			contentPanel.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{412, 0};
			gbl_panel.rowHeights = new int[]{23, 0};
			gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				chckbxAutoUpdateString = new JCheckBox("Auto update String size");
				chckbxAutoUpdateString.setSelected(true);
				chckbxAutoUpdateString.setHorizontalAlignment(SwingConstants.LEFT);
				GridBagConstraints gbc_chckbxAutoUpdateString = new GridBagConstraints();
				gbc_chckbxAutoUpdateString.fill = GridBagConstraints.BOTH;
				gbc_chckbxAutoUpdateString.gridx = 0;
				gbc_chckbxAutoUpdateString.gridy = 0;
				panel.add(chckbxAutoUpdateString, gbc_chckbxAutoUpdateString);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{79, 0, 59, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton btnReplaceAll = new JButton("Replace All");
				btnReplaceAll.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						doReplace();
					}
				});
				btnReplaceAll.setActionCommand("Cancel");
				GridBagConstraints gbc_btnReplaceAll = new GridBagConstraints();
				gbc_btnReplaceAll.anchor = GridBagConstraints.NORTHWEST;
				gbc_btnReplaceAll.insets = new Insets(5, 5, 5, 5);
				gbc_btnReplaceAll.gridx = 0;
				gbc_btnReplaceAll.gridy = 0;
				buttonPane.add(btnReplaceAll, gbc_btnReplaceAll);
			}
			{
				JButton closeButton = new JButton("Close");
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						doClose();
					}
				});
				closeButton.setActionCommand("Cancel");
				GridBagConstraints gbc_closeButton = new GridBagConstraints();
				gbc_closeButton.insets = new Insets(5, 5, 5, 5);
				gbc_closeButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_closeButton.gridx = 2;
				gbc_closeButton.gridy = 0;
				buttonPane.add(closeButton, gbc_closeButton);
			}
		}
	}

	protected abstract void doReplace();
	
	protected abstract void doClose();
	
	protected abstract void doSearchPasteSel();
	
	protected abstract void doReplacePasteSel();
	
	protected JTextField getTextSearchString() {
		return textSearchedString;
	}
	
	protected JTextField getTextReplaceString() {
		return textField;
	}
	
	protected JRadioButton getRdbtnSelection() {
		return rdbtnSelection;
	}
	
	protected JRadioButton getRdbtnChunk() {
		return rdbtnChunk;
	}
	
	protected JRadioButton getRdbtnAllFile() {
		return rdbtnAllFile;
	}
	protected JRadioButton getRdbtnAllFileAndName() {
		return rdbtnAllFileAndName;
	}
	
	protected JCheckBox getChckbxAutoUpdateString() {
		return chckbxAutoUpdateString;
	}
}
