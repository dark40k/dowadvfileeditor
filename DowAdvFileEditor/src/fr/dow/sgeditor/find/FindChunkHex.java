package fr.dow.sgeditor.find;

import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.sgeditor.FileFormatEditorUtilities;
import fr.dow.sgeditor.chunkeditor.ChunkEditor;
import fr.dow.sgeditor.treechunk.TreeChunk;
import fr.dow.sgeditor.treechunk.TreeChunkNode;

public class FindChunkHex extends FindChunkHexBase {

	private TreeChunk treeChunk;
	private ChunkEditor chunkEditor;

	/**
	 * Launch the application.
	 * 
	 * @param chunkEditor
	 */
	public static void showDialog(Window owner, TreeChunk treeChunk, ChunkEditor chunkEditor) {
		try {
			FindChunkHex dialog = new FindChunkHex(owner, treeChunk, chunkEditor);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FindChunkHex(Window owner, TreeChunk treeChunk, ChunkEditor chunkEditor) {
		super(owner);
		this.treeChunk = treeChunk;
		this.chunkEditor = chunkEditor;
	}

	@Override
	protected void doFindNext() {

		String searchString = getTextSearchedString().getText();
		
		if ((searchString.length() & 1) !=0) {
			JOptionPane.showMessageDialog(null, "Cannot read : Odd number of character", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		byte[] pattern = new byte [searchString.length()/2];
				
		try {
			for (int i = 0; i < searchString.length() / 2; i++)
				pattern[i] = (byte) Integer.parseInt(searchString.substring(i * 2, i * 2 + 2), 16);
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Cannot read : character not hexadecimal", "Error", JOptionPane.ERROR_MESSAGE);
			return;		
		}
		
		if (getRdbtnSelection().isSelected()) {
			doFindNextInSelection(pattern);
			getRdbtnCompleteFile().setSelected(true);
			return;
		}

		doFindNextChunk(pattern);

	}

	private void doFindNextChunk(byte[] pattern) {

		int index = -1;

		{
			RelicChunk curChunk = chunkEditor.getChunk();
			if (curChunk != null)
				if (curChunk.get_type().equals(RelicChunkDATA.type)) {
					index = FileFormatEditorUtilities.findData((RelicChunkDATA) curChunk, pattern,
							chunkEditor.getHexSelectionStart());
					if (index == chunkEditor.getHexSelectionStart())
						index = FileFormatEditorUtilities.findData((RelicChunkDATA) curChunk, pattern,
								chunkEditor.getHexSelectionStart() + 1);
					if (index >= 0) {
						chunkEditor.setHexSelection(index, index + pattern.length - 1);
						return;
					}
				}
		}

		TreeChunkNode curChunkNode = treeChunk.getLastSelectedNode();
		if (curChunkNode == null) curChunkNode = (TreeChunkNode) treeChunk.getTreeModel().getRoot();

		while (index < 0) {

			curChunkNode = curChunkNode.next();
			if (curChunkNode == null) return;

			if (curChunkNode.getChunk().get_type().equals(RelicChunkDATA.type))
				index = FileFormatEditorUtilities.findData((RelicChunkDATA) curChunkNode.getChunk(), pattern);

		}

		treeChunk.getTree().scrollPathToVisible(curChunkNode.getTreePath());
		treeChunk.getTree().setSelectionPath(curChunkNode.getTreePath());

		chunkEditor.setHexSelection(index, index + pattern.length - 1);

	}

	private void doFindNextInSelection(byte[] pattern) {

		RelicChunk curChunk = chunkEditor.getChunk();

		if (curChunk == null) return;
		if (!curChunk.get_type().equals(RelicChunkDATA.type)) return;

		int index = FileFormatEditorUtilities.findData((RelicChunkDATA) curChunk, pattern,
				chunkEditor.getHexSelectionStart(), chunkEditor.getHexSelectionEnd());

		if (index < 0) return;

		chunkEditor.setHexSelection(index, index + pattern.length - 1);

	}

	@Override
	protected void doClose() {
		this.setVisible(false);
	}


	@Override
	protected void doPasteSel() {
		getTextSearchedString().setText(chunkEditor.getSelectionAsHexString());
	}

}
