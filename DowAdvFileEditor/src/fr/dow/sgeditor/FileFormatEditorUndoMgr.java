package fr.dow.sgeditor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import fr.dow.gamedata.RelicChunkFile;

public class FileFormatEditorUndoMgr {
	
	private static Integer MAX_UNDO_EVENTS = 25;
	
	private UndoManager undoMgr = new UndoManager();

	private RelicChunkFile chunkFile;
	
	private FileFormatEditor ffEditor;
	
	public FileFormatEditorUndoMgr(FileFormatEditor ffEditor) {
		this.ffEditor=ffEditor;
		undoMgr.setLimit(MAX_UNDO_EVENTS);
	}

	// -------------------------------------------------------------------------------------------
	//
	// Commandes
	//
	// -------------------------------------------------------------------------------------------
	
	public void setChunkFile(RelicChunkFile chunkFile) {
		discardAllEdits();
		this.chunkFile=chunkFile;
	}
	
	// Undo
	
	public boolean canUndo() {
		return undoMgr.canUndo();
	}

	public void undo() {
		System.out.println(undoMgr.getUndoPresentationName());
		undoMgr.undo();
	}

	// Redo
	
	public boolean canRedo() {
		return undoMgr.canRedo();
	}

	public void redo() {
		System.out.println(undoMgr.getRedoPresentationName());
		undoMgr.redo();
	}

	// Manage pile
	
	public void discardAllEdits() {
		undoMgr.discardAllEdits();
	}

	public void addEdit(UndoableNodeDatabaseEdit edit) {
		undoMgr.addEdit(edit);
	}

	public String getUndoPresentationName() {
		if (canUndo()) return undoMgr.getUndoPresentationName();
		return "Undo <empty>";
	}
	
	public String getRedoPresentationName() {
		if (canRedo()) return undoMgr.getRedoPresentationName();
		return "Redo <empty>";
	}
	
	// Database edit
	
	public UndoableNodeDatabaseEdit createUndoableGlobalEdit(String actionName) {
		return new UndoableGlobalEdit(actionName);
	}
	
	public void addUndoableGlobalEdit(String actionName) {
		undoMgr.addEdit(createUndoableGlobalEdit(actionName));
	}
	
	// -------------------------------------------------------------------------------------------
	//
	// Edition globale de la base
	//
	// -------------------------------------------------------------------------------------------
	
	public interface UndoableNodeDatabaseEdit extends UndoableEdit {}
	
	public class UndoableGlobalEdit implements UndoableNodeDatabaseEdit {
		
		private byte[] undoChunkFile;
		private ArrayList<Integer> undoExpansionState;
		
		private byte[] redoChunkFile;
		private ArrayList<Integer> redoExpansionState;

		private String actionName;
		
		public UndoableGlobalEdit(String actionName) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				chunkFile.writeObject(bos);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Can't store object");
			}
			undoChunkFile = bos.toByteArray();
		
			undoExpansionState = ffEditor.getTreeChunk().saveRowExpansionState();
			
			this.actionName=actionName;
		}

		@Override
		public void undo() throws CannotUndoException {
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				chunkFile.writeObject(bos);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Can't store object");
			}
			redoChunkFile = bos.toByteArray();
			redoExpansionState = ffEditor.getTreeChunk().saveRowExpansionState();
			
			try {
				chunkFile.readObject(new ByteArrayInputStream(undoChunkFile));
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Can't fetch object");
			}
			ffEditor.getTreeChunk().forceCompleteUpdate();
			
			ffEditor.getTreeChunk().loadRowExpansionState(undoExpansionState);

		}

		@Override
		public boolean canUndo() {
			return (actionName!=null);
		}

		@Override
		public void redo() throws CannotRedoException {
			if (!canRedo()) throw new CannotRedoException();
			try {
				chunkFile.readObject(new ByteArrayInputStream(redoChunkFile));
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Can't fetch object");
			}
			ffEditor.getTreeChunk().forceCompleteUpdate();
			
			ffEditor.getTreeChunk().loadRowExpansionState(redoExpansionState);
			
		}

		@Override
		public boolean canRedo() {
			return (redoChunkFile!=null);
		}

		@Override
		public void die() {
		}

		@Override
		public boolean addEdit(UndoableEdit anEdit) {
			return false;
		}

		@Override
		public boolean replaceEdit(UndoableEdit anEdit) {
			return false;
		}

		@Override
		public boolean isSignificant() {
			return true;
		}

		@Override
		public String getPresentationName() {
			return actionName;
		}

		@Override
		public String getUndoPresentationName() {
			return "Undo "+actionName;
		}

		@Override
		public String getRedoPresentationName() {
			return "Redo "+actionName;
		}	
	}
}
